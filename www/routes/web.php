<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes(['register' => false]);

//Route::get('/home', 'HomeController@index')->name('home');

// Requests Web
Route::prefix('web')->group(function () {
	// Painel
	Route::prefix('painel')->group(function () {
		Route::get('data', 'HomeController@data');
	});

	// Roles
	Route::prefix('roles')->group(function () {
		Route::get('data', 'RoleController@data');
	});

	// Perfil
	Route::prefix('perfil')->group(function () {
		Route::get('show', 'UserController@showOwnUser');
		Route::post('update-name', 'UserController@updateOwnName');
		Route::post('update-password', 'UserController@updateOwnPassword');
		Route::delete('destroy', 'UserController@destroyOwnCount');
	});
	// Users
	Route::prefix('users')->group(function () {
		Route::get('data', 'UserController@data');
		Route::get('show/{user}', 'UserController@show');
		Route::get('permissions', 'UserController@showPermissions');
		Route::post('store', 'UserController@store');
		Route::post('update/{user}', 'UserController@update');
		Route::delete('destroy/{user}', 'UserController@destroy');
	});
    // Clientes
    Route::prefix('clientes')->group(function () {
		Route::get('data', 'ClienteController@data');
		Route::get('show/{cliente}', 'ClienteController@show');
		Route::post('store', 'ClienteController@store');
		Route::post('update/{cliente}', 'ClienteController@update');
		Route::delete('destroy/{cliente}', 'ClienteController@destroy');
	});
	// Endereços
    Route::prefix('enderecos')->group(function () {
    	Route::get('data', 'EnderecoController@data');
		Route::get('data/{cliente}', 'EnderecoController@dataCliente');
		Route::get('show/{endereco}', 'EnderecoController@show');
		Route::post('set-principal/{endereco}', 'EnderecoController@setPrincipal');
		Route::post('store', 'EnderecoController@store');
		Route::post('update/{endereco}', 'EnderecoController@update');
		Route::delete('destroy/{endereco}', 'EnderecoController@destroy');
	});
});

Route::get('/{path?}', 'SpaController@index')->where('path', '.*')->name('spa.index');


