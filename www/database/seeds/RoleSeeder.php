<?php

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;

class RoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

    	// Admin
        $role = Role::create(['name' => 'Admin']);
        $role->givePermissionTo(Permission::all());

        // Editor
        $role = Role::create(['name' => 'Editor']);
        $role->givePermissionTo('Visualizar Clientes');
        $role->givePermissionTo('Cadastrar Clientes');
        $role->givePermissionTo('Editar Clientes');
        $role->givePermissionTo('Excluir Clientes');
        $role->givePermissionTo('Visualizar Endereços');
        $role->givePermissionTo('Cadastrar Endereços');
        $role->givePermissionTo('Editar Endereços');
        $role->givePermissionTo('Excluir Endereços');

        // Visualizador
        $role = Role::create(['name' => 'Visualizador']);
        $role->givePermissionTo('Visualizar Clientes');
        $role->givePermissionTo('Visualizar Endereços');

    }
}
