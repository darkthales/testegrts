<?php

use Illuminate\Database\Seeder;
use App\Models\Endereco;
use App\Models\Cliente;

class EnderecoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(Endereco::class, 500)->create();
        $clientes = Cliente::all();
        foreach ($clientes as $value) {
        	$endereco = Endereco::where('cliente_id', $value->id)->first();
            if(!empty($endereco)){
                $endereco->principal = true;
                $endereco->save();
            }
        }
    }
}
