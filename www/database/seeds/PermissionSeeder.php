<?php

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Permission;

class PermissionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        // Reset cached roles and permissions
        app()[\Spatie\Permission\PermissionRegistrar::class]->forgetCachedPermissions();

        // Roles / Roles
        Permission::create(['name' => 'Visualizar Perfis']);

        // Usuários
        Permission::create(['name' => 'Visualizar Usuários']);
        Permission::create(['name' => 'Cadastrar Usuários']);
        Permission::create(['name' => 'Editar Usuários']);
        Permission::create(['name' => 'Excluir Usuários']);

        // Clientes
        Permission::create(['name' => 'Visualizar Clientes']);
        Permission::create(['name' => 'Cadastrar Clientes']);
        Permission::create(['name' => 'Editar Clientes']);
        Permission::create(['name' => 'Excluir Clientes']);

        // Endereço
        Permission::create(['name' => 'Visualizar Endereços']);
        Permission::create(['name' => 'Cadastrar Endereços']);
        Permission::create(['name' => 'Editar Endereços']);
        Permission::create(['name' => 'Excluir Endereços']);
    }
}
