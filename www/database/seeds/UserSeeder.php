<?php

use Illuminate\Database\Seeder;
use App\User;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            [
                'name' => 'Admin',
                'email' => 'admin@grts.com.br',
                'password' => bcrypt('grtspassword')
            ],
            [
                'name' => 'Editor',
                'email' => 'editor@grts.com.br',
                'password' => bcrypt('grtspassword')
            ],
            [
                'name' => 'Visualizador',
                'email' => 'visualizador@grts.com.br',
                'password' => bcrypt('grtspassword')
            ],
		]);

        $user = User::find(1);
        $user->assignRole('Admin');

        $user = User::find(2);
        $user->assignRole('Editor');

        $user = User::find(3);
        $user->assignRole('Visualizador');

        //$this->runFactory(10);

    }

    private function runFactory($value){
        $perfis = [
            'Admin',
            'Editor',
            'Visualizador'
        ];

        factory(App\User::class, $value)->create();

        $users = User::all();

        for ($i=3; $i<($value+3); $i++) { 
            $randon = rand(0, 2);
            $users[$i]->assignRole($perfis[$randon]);
        }
    }
}
