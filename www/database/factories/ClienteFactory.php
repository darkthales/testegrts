<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Cliente;
use Faker\Generator as Faker;
use Illuminate\Support\Str;

$factory->define(Cliente::class, function (Faker $faker) {
	$cnpj = '';
    for($i=1; $i<=17; $i++){
        $cnpj .= $faker->randomDigit;
    }
    return [
        'nome_empresa' => $faker->name,
        'cnpj' => $cnpj,
        'telefone' => $faker->phoneNumber,
        'nome_responsavel' => $faker->name,
        'email' => $faker->unique()->safeEmail
    ];
});
