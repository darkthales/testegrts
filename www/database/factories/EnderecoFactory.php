<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Endereco;
use App\Models\Cliente;
use Faker\Generator as Faker;
use Illuminate\Support\Str;

$factory->define(Endereco::class, function (Faker $faker) {
    $cep = '';
    for($i=1; $i<=8; $i++){
        $cep .= $faker->randomDigit;
    }
    return [
        'cep' => $cep,
        'estado' => $faker->city,
        'cidade' => $faker->city,
        'bairro' => $faker->city,
        'logradouro' => $faker->city,
        'complemento' => $faker->city,
        'numero' => $faker->randomDigit,
        'principal' => false,
        'cliente_id' => rand(1, Cliente::count())
    ];
});
