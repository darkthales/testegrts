<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\Http\Requests\Users\StoreRequest;
use App\Http\Requests\Users\UpdateRequest;
use App\Http\Requests\Users\PasswordRequest;
use App\Http\Requests\Users\NameRequest;
use Spatie\Permission\Models\Permission;
use App\User;
use App\Http\Message;

class UserController extends Controller
{
    public function __construct(){
        $this->middleware('auth');
    }

    public function data(){
        try {
            if($this->can('Visualizar Usuários')){
                return response()->json(User::all());
            } else {
                return Message::error($this->notHavePermissionMessage, 403);
            }
        } catch(\Exception $e){
            return Message::error($e->getMessage());
        }
    }

    public function show(User $user){
        try {
            if($this->can('Visualizar Usuários')){
                return response()->json($user);
            } else {
                return Message::error($this->notHavePermissionMessage, 403);
            }
        } catch(\Exception $e){
            return Message::error($e->getMessage());
        }
    }

    public function showPermissions(){
        try {
            if(!empty(Auth::user())){
                $dados = $this->setAllPermissionsFalse();
                $userPermissions = Auth::user()->getAllPermissions();
                foreach ($userPermissions as $permission) {
                    $dados[$permission->name] = true;
                }
                return response()->json($dados);
            }
        } catch(\Exception $e){
            return Message::error($e->getMessage());
        }
    }

    private function setAllPermissionsFalse(){
        $dados = [];
        $allPermissions = Permission::get();
        foreach ($allPermissions as $permission) {
            $dados += [
                $permission->name => false
            ];
        }
        return $dados;
    }

    public function showOwnUser(){
        try {
            return response()->json(Auth::user());
        } catch(\Exception $e){
            return Message::error($e->getMessage());
        }
    }

    public function updateOwnName(NameRequest $request){
        try {
            $user = Auth::user();
            $user->update($request->only('name'));
            return Message::success('Nome editado com sucesso.');
        } catch(\Exception $e){
            return Message::error($e->getMessage());
        } 
    }

    public function updateOwnPassword(PasswordRequest $request){
        try {
            $user = Auth::user();
            $user->update($request->only('password'));
            return Message::success('Senha editada com sucesso.');
        } catch(\Exception $e){
            return Message::error($e->getMessage());
        } 
    }

    public function store(StoreRequest $request){
        try {
            if($this->can('Cadastrar Usuários')){
                $user = User::create($request->all());
                $user->assignRole($request->perfil);
                return Message::success('O usuário foi cadastrado com sucesso.');
            } else {
                return Message::error($this->notHavePermissionMessage, 403);
            }
        } catch(\Exception $e){
            return Message::error($e->getMessage());
        }    
    }

    public function update(UpdateRequest $request, User $user){
        try {
            if($this->can('Editar Usuários')){
                $user->update($request->all());
                $user->removeRole($user->perfil);
                $user->assignRole($request->perfil);
                return Message::success('O usuário foi editado com sucesso.');
            } else {
                return Message::error($this->notHavePermissionMessage, 403);
            }
        } catch(\Exception $e){
            return Message::error($e->getMessage());
        }  
    }

    public function destroy(User $user){
        try {
            if($this->can('Excluir Usuários')){
                $user->delete();
                return Message::success('O usuário foi removido com sucesso.');
            } else {
                return Message::error($this->notHavePermissionMessage, 403);
            }
        } catch(\Exception $e){
            return Message::error($e->getMessage());
        }    
    }

    public function destroyOwnCount(){
        try {
            $user = Auth::user();
            Auth::logout();
            $user->delete();
            return Message::success('O usuário foi removido com sucesso.');
        } catch(\Exception $e){
            return Message::error($e->getMessage());
        }    
    }
}
