<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\ClienteRequest;
use App\Models\Cliente;
use App\Models\Endereco;
use App\Http\Message;

class ClienteController extends Controller
{
    public function __construct(){
        $this->middleware('auth');
    }

    public function data(){
        try {
            if($this->can('Visualizar Clientes')){
                $clientes = Cliente::all();
                return response()->json($clientes);
            } else {
                return Message::error($this->notHavePermissionMessage, 403);
            }
        } catch(\Exception $e){
            return Message::error($e->getMessage());
        }
    }

    public function show(Cliente $cliente){
        try {
            if($this->can('Visualizar Clientes')){
                return response()->json($cliente);
            } else {
                return Message::error($this->notHavePermissionMessage, 403);
            }
        } catch(\Exception $e){
            return Message::error($e->getMessage());
        }
    }

    public function store(ClienteRequest $request){
        try {
            if($this->can('Cadastrar Clientes')){
                Cliente::create($request->all());
                return Message::success('O cliente foi cadastrado com sucesso.');
            } else {
                return Message::error($this->notHavePermissionMessage, 403);
            }
        } catch(\Exception $e){
            return Message::error($e->getMessage());
        }    
    }

    public function update(ClienteRequest $request, Cliente $cliente){
        try {
            if($this->can('Editar Clientes')){
                $cliente->update($request->all());
                return Message::success('O cliente foi editado com sucesso.');
            } else {
                return Message::error($this->notHavePermissionMessage, 403);
            }
        } catch(\Exception $e){
            return Message::error($e->getMessage());
        }    
    }

    public function destroy(Cliente $cliente){
        try {
            if($this->can('Excluir Clientes')){
                $this->destroyEnderecos($cliente->id);
                $cliente->delete();
                return Message::success('O cliente foi removido com sucesso.');
            } else {
                return Message::error($this->notHavePermissionMessage, 403);
            }
        } catch(\Exception $e){
            return Message::error($e->getMessage());
        }    
    }

    private function destroyEnderecos($cliente_id){
        $enderecos = Endereco::where('cliente_id', $cliente_id)->get();
        foreach ($enderecos as $value) {
            $value->delete();
        }
    }
}
