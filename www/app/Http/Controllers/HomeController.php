<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Endereco;
use App\Models\Cliente;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function data(){
        try {
            if($this->can('Visualizar Clientes') && $this->can('Visualizar Endereços')){
                $data = [
                    'total_clientes' => Cliente::count(),
                    'total_enderecos' => Endereco::count()
                ];
                return response()->json($data);
            } else {
                return Message::error($this->notHavePermissionMessage, 403);
            }
        } catch(\Exception $e){
            return Message::error($e->getMessage());
        }
    }
}
