<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Message;
use Spatie\Permission\Models\Role;

class RoleController extends Controller
{
    public function __construct(){
        $this->middleware('auth');
    }

    public function data(){
    	try {
            if($this->can('Visualizar Perfis')){
            	$roles = Role::all();
                return response()->json($roles);
            } else {
                return Message::error($this->notHavePermissionMessage, 403);
            }
        } catch(\Exception $e){
            return Message::error($e->getMessage());
        }
    }
}
