<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\EnderecoRequest;
use App\Models\Endereco;
use App\Models\Cliente;
use App\Http\Message;

class EnderecoController extends Controller
{
    public function __construct(){
        $this->middleware('auth');
    }

    public function data(){
        try {
            if($this->can('Visualizar Endereços')){
                $enderecos = Endereco::all();
                return response()->json($enderecos);
            } else {
                return Message::error($this->notHavePermissionMessage, 403);
            }
        } catch(\Exception $e){
            return Message::error($e->getMessage());
        }
    }

    public function dataCliente($cliente){
        try {
            if($this->can('Visualizar Endereços')){
                $enderecos = Endereco::where('cliente_id', $cliente)->get();
                return response()->json($enderecos);
            } else {
                return Message::error($this->notHavePermissionMessage, 403);
            }
        } catch(\Exception $e){
            return Message::error($e->getMessage());
        }
    }

    public function show(Endereco $endereco){
        try {
            if($this->can('Visualizar Endereços')){
                return response()->json($endereco);
            } else {
                return Message::error($this->notHavePermissionMessage, 403);
            }
        } catch(\Exception $e){
            return Message::error($e->getMessage());
        }
    }

    public function store(EnderecoRequest $request){
        try {
            if($this->can('Cadastrar Endereços')){
                $endereco = Endereco::create($request->all());
                if($this->primeiroEnderecoCadastrado($request->cliente_id) or $request->principal){
                    $this->setPrincipal($endereco);
                }
                return Message::success('O endereço foi cadastrado com sucesso.');
            } else {
                return Message::error($this->notHavePermissionMessage, 403);
            }
        } catch(\Exception $e){
            return Message::error($e->getMessage());
        }    
    }

    public function update(EnderecoRequest $request, Endereco $endereco){
        try {
            if($this->can('Editar Endereços')){
                $endereco->update($request->all());
                if($request->principal){
                    $this->setPrincipal($endereco);
                }
                return Message::success('O endereço foi editado com sucesso.');
            } else {
                return Message::error($this->notHavePermissionMessage, 403);
            }
        } catch(\Exception $e){
            return Message::error($e->getMessage());
        }    
    }

    private function primeiroEnderecoCadastrado($cliente_id){
        $count = Endereco::where('cliente_id', $cliente_id)->count();
        if($count == 1){
            return true;
        } else {
            return false;
        }
    }

    public function setPrincipal(Endereco $endereco){
        try {
            if($this->can('Cadastrar Endereços') or $this->can('Editar Endereços')){
                $this->setAllNotPrincipal($endereco);
                $endereco->principal = true;
                $endereco->save();
                return Message::success('Este endereço é o principal.');
            } else {
                return Message::error($this->notHavePermissionMessage, 403);
            }
        } catch(\Exception $e){
            return Message::error($e->getMessage());
        } 
    }

    private function setAllNotPrincipal(Endereco $endereco){
        $enderecos = Endereco::where('id', '!=', $endereco->id)
            ->where('cliente_id', $endereco->cliente_id)
            ->where('principal', true)
            ->get();
        foreach($enderecos as $value) {
            $value->principal = false;
            $value->save();
        }
    }

    public function destroy(Endereco $endereco){
        try {
            if($this->can('Excluir Endereços')){
                if($endereco->principal){
                    $this->setNovoEnderecoPrincipal($endereco->cliente_id, $endereco->id);
                }
                $endereco->delete();
                return Message::success('O endereço foi removido com sucesso.');
            } else {
                return Message::error($this->notHavePermissionMessage, 403);
            }
        } catch(\Exception $e){
            return Message::error($e->getMessage());
        }    
    }

    private function setNovoEnderecoPrincipal($cliente_id, $endereco_id){
        if(!$this->primeiroEnderecoCadastrado($cliente_id)){
            $endereco = Endereco::where('cliente_id', $cliente_id)
                ->where('id', '!=', $endereco_id)
                ->first();
            $this->setPrincipal($endereco);
        }
    }
}
