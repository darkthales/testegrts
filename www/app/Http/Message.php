<?php
namespace App\Http;

class Message
{
    public static function get($message, $type, $success, $code=200){
        return response()->json([
            'success'=>$success,
            'type' => $type,
            'message' => $message,
        ], 200);
    }

    public static function success($message=''){
    	if(!empty($message)){
    		return response()->json([
                'success'=>true,
                'type' => 'success',
                'message' => $message,
        	], 200);
    	} else {
    		return response()->json([
                'success'=>true,
                'type' => 'success',
                'message' => 'A operação foi realizada com sucesso!',
        	], 200);
    	}
    }

    public static function error($erro='', $code=500){
    	if(!empty($erro)){
    		return response()->json([
                'success'=>false,
                'type' => 'error',
                'message' => $erro,
        	], $code);
    	} else {
    		return response()->json([
                'success'=>false,
                'type' => 'error',
                'message' => 'Um erro inesperado aconteceu!',
        	], $code);
    	}
    }
}