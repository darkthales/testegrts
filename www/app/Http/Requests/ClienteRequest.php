<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use App\Library\Validation;

class ClienteRequest extends FormRequest
{
    use Validation;

    public $permissions = [
        'create' => 'Cadastrar Clientes',
        'update' => 'Editar Clientes'
    ];

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return $this->userCan($this->permissions[$this->getRequestMethod('cliente')]);
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'nome_empresa' => 'required|min:3|max:255|unique:clientes,nome_empresa,'.$this->getDataId('cliente'),
            'cnpj' => 'required|cnpj|unique:clientes,cnpj,'.$this->getDataId('cliente'),
            'email' => 'required|email|max:255|unique:clientes,email,'.$this->getDataId('cliente'),
            'nome_responsavel' => 'required|max:255',
            'telefone' => 'required|min:14|max:15'
        ];
    }
}
