<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use App\Library\EnderecoValidation;
use App\Library\Validation;
use App\Models\Endereco;

class EnderecoRequest extends FormRequest
{

    use Validation;
    use EnderecoValidation;

    public $permissions = [
        'create' => 'Cadastrar Endereços',
        'update' => 'Editar Endereços'
    ];
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return $this->userCan($this->permissions[$this->getRequestMethod('endereco')]);
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(){
        return [
            'cep' => 'required|size:9',
            'estado' => 'required',
            'cidade' => 'required',
            'bairro' => 'required',
            'logradouro' => 'required',
            'numero' => 'required',
            'complemento' => 'required',
            'cliente_id' => 'required',
            'principal' => 'required'
        ];
    }

    public function withValidator($validator)
    {
        if(!$validator->fails()){
            // Regra: Cliente não pode cadastrar o mesmo cep 2 vezes
            /*if(!$this->cepIsUniqueForSameCliente()){
                $validator->after(function ($validator){
                    $validator->errors()->add('cep', 'Este CEP já foi cadastrado para este cliente.');
                });
            }*/

            // Regra: Cliente não pode cadastrar o mesmo endereço 2 vezes (mesmo cep, complemento e número)
            if(!$this->enderecoIsUniqueForSameCliente()){
                $validator->after(function ($validator){
                    $validator->errors()->add('cep', 'Este endereço já foi cadastrado para este cliente.');
                });
            }
            $this->validarEndereco($validator);
        }
    }

    private function cepIsUniqueForSameCliente(){
        $id = $this->getDataId('endereco');
        $count = Endereco::where('cliente_id', $this->cliente_id)
            ->where('cep', $this->cep)
            ->where('id', '!=', $id)
            ->count();
        if($count == 0){
            return true;
        } else {
            return false;
        }
    }

    private function enderecoIsUniqueForSameCliente(){
        $id = $this->getDataId('endereco');
        $count = Endereco::where('cliente_id', $this->cliente_id)
            ->where('cep', $this->cep)
            ->where('complemento', $this->complemento)
            ->where('numero', $this->numero)
            ->where('id', '!=', $id)
            ->count();
        if($count == 0){
            return true;
        } else {
            return false;
        }
    }
}
