<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Cliente extends Model
{
    protected $fillable = [
        'nome_empresa', 
        'cnpj', 
        'telefone',
        'nome_responsavel',
        'email'
    ];

    protected $appends = [
        'endereco_principal'
    ];

    // Endereco Principal
    public function getEnderecoPrincipalAttribute(){
        return Endereco::where('cliente_id', $this->id)
            ->where('principal', true)->first();
    }

    // 
    public function enderecos()
    {
        return $this->hasMany(Endereco::class, 'id', 'cliente_id');
    }
}
