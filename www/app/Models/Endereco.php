<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Endereco extends Model
{
    protected $fillable = [
        'cep',
        'estado',
        'cidade',
        'bairro',
        'logradouro',
        'complemento',
        'numero',
        'cliente_id',
        'principal'
    ];

    protected $appends = [
        'cliente'
    ];

    // Cliente
    public function getClienteAttribute(){
        return Cliente::find($this->cliente_id)->nome_empresa;
    }

}
