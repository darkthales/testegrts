<?php

namespace App\Library;

trait EnderecoValidation
{
    private $viaCep;

    private function validarEndereco($validator){
        $this->getViaCep();
        if(!$this->cepIsValid()){
            $validator->after(function ($validator){
                $validator->errors()->add('cep', 'CEP inválido');
            });
        } else {
            if(!$this->stateIsValid()){
	            $validator->after(function ($validator){
	                $validator->errors()->add('estado', 'Estado inválido, o estado encontrado foi '.$this->viaCep->uf);
	            });
	        }
	        if(!$this->cityIsValid()){
	            $validator->after(function ($validator){
	                $validator->errors()->add('cidade', 'Cidade inválida, a cidade encontrada foi '.$this->viaCep->localidade);
	            });
	        }
	        if(!$this->neighborhoodIsValid() && !empty($this->viaCep->bairro)){
	            $validator->after(function ($validator){
	                $validator->errors()->add('bairro', 'Bairro inválido, o bairro encontrado foi '.$this->viaCep->bairro);
	            });
	        }
	        if(!$this->localityIsValid() && !empty($this->viaCep->logradouro)){
	            $validator->after(function ($validator){
	                $validator->errors()->add('logradouro', 'Localidade inválida, o logradouro encontrado foi '.$this->viaCep->logradouro);
	            });
	        }
        }
    }

    private function getViaCep(){
        try {
            $output = json_decode(file_get_contents("https://viacep.com.br/ws/".$this->cep."/json/"));
            $this->viaCep = $output;
        } catch(\Exception $e){
            $this->viaCep = [];
        }
    }

    private function cepIsValid(){
        if(!empty($this->viaCep) && !property_exists($this->viaCep, 'erro')){
            return true;
        } else {
            return false;
        }
    }

    private function stateIsValid(){
        if($this->estado == $this->viaCep->uf){
            return true;
        } else {
            return false;
        }
    }

    private function cityIsValid(){
        if($this->cidade == $this->viaCep->localidade){
            return true;
        } else {
            return false;
        }
    }

    private function neighborhoodIsValid(){
        if($this->bairro == $this->viaCep->bairro){
            return true;
        } else {
            return false;
        }
    }

    private function localityIsValid(){
        if($this->logradouro == $this->viaCep->logradouro){
            return true;
        } else {
            return false;
        }
    }
}