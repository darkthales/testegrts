<?php

namespace App\Library;

use Illuminate\Support\Facades\Auth;

trait Validation
{
    /*
        Usado para ignorar o proprio elemento caso o método desejado seja o update 
        e passar nas validações como unique
        return 0 -> create
        return !0 -> update
    */
    public function getDataId($data){
        $data = $this->route($data);
        if(!empty($data)) {
            $data = $data->id;
        } else {
            $data = 0;
        } 
        return $data;
    }

    public function getRequestMethod($data){
        if($this->getDataId($data) > 0){
            return 'update';
        } else {
            return 'create';
        }
    }

    public function userCan($permission){
        if(Auth::user()->can($permission)){
            return true;
        } else {
            return false;
        }
    }
}