export default class Cliente {
	constructor(
		nome_empresa='',
		cnpj='',
		telefone='',
		nome_responsavel='',
		email=''
	){
		this.nome_empresa = nome_empresa;
		this.cnpj = cnpj;
		this.telefone = telefone;
		this.nome_responsavel = nome_responsavel;
		this.email = email;
	}
}