export default class User {
	constructor(
		name='',
		email='',
		password='',
		perfil=''
	){
		this.name = name;
		this.email = email;
		this.password = password;
		this.perfil = perfil;
	}
}