export default class Endereco {
	constructor(
		cep='',
		estado='',
		cidade='',
		bairro='',
		logradouro='',
		complemento='',
		numero='',
		cliente_id='',
		principal=false
	){
		this.cep = cep;
		this.estado = estado;
		this.cidade = cidade;
		this.bairro = bairro;
		this.logradouro = logradouro;
		this.complemento = complemento;
		this.numero = numero;
		this.cliente_id = cliente_id;
		this.principal = principal;
	}
}