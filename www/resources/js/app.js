require('./bootstrap');

window.Vue = require('vue');

// Rotas do SPA
import VueRouter from 'vue-router'
Vue.use(VueRouter)

// Máscara para formulários
import VueTheMask from 'vue-the-mask'
Vue.use(VueTheMask)

//import componente 
import GrtsVue from './grtsLibrary/index'
Vue.use(GrtsVue)

// Importando compoentes
const files = require.context('./', true, /\.vue$/i)
files.keys().map(key => Vue.component(key.split('/').pop().split('.')[0], files(key).default))

// Prototypes
Vue.prototype.$url = APP_URL;

import getRoutes from './routes'
 
async function start(){
  const routes = await getRoutes();
  const router = new VueRouter({
    mode: 'history',
    base: '/',
    routes
  })

  	// Instance vue
  	const app = new Vue({
	    el: '#app',
	    router
	});
}

start();
