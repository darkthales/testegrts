export default class Auth {
	async getPermission(){
		const promises = axios.get(APP_URL+'/web/users/permissions');
		const response = await promises;
		this.permissions = response.data;
	}
	can(permission){
		if(this.permissions != undefined){
			if(this.permissions[permission]){
				return true;
			} else {
				return false;
			}
		} else {
			return false;
		}
	}
}
