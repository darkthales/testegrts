import Auth from './middleware/Auth.js'

import Painel from './pages/painel/Index'

import Error404 from './pages/errors/Error404'
import Error403 from './pages/errors/Error403'

import Cliente from './pages/clientes/Index'
import ClienteShow from './pages/clientes/Show'

import Endereco from './pages/enderecos/Index'

import User from './pages/users/Index'

import Perfil from './pages/perfil/Index'

export default async function getRoutes(){
    let auth = new Auth();
    await auth.getPermission();
    Vue.prototype.$auth = auth;
    const routes = [
    	// Errors
        { path: '*', component: Error404 },
        { path: '/403', component: Error403 },

        //Painel
        {
            path: '/',
            name: 'home',
            component: Painel,
        },
        {
            path: '/painel',
            name: 'painel',
            component: Painel,
        },

        // Perfil
        {
            path: '/perfil',
            name: 'perfil',
            component: Perfil,
        },

        // User
        {
            path: '/users',
            name: 'users',
            component: User,
            linkExactActiveClass: 'active',
            beforeEnter: (to, from, next) => {
                if(!auth.can('Visualizar Usuários')) {
                    next({
                        path: '/403',
                    })
                } else {
                    next()
                }
            },
        },

        // Cliente
        {
            path: '/clientes',
            name: 'clientes',
            component: Cliente,
            linkExactActiveClass: 'active',
            beforeEnter: (to, from, next) => {
                if(!auth.can('Visualizar Clientes')) {
                    next({
                        path: '/403',
                    })
                } else {
                    next()
                }
            },
        },
        {
            path: '/clientes/show/:id',
            name: 'clienteShow',
            component: ClienteShow,
            linkExactActiveClass: 'active',
            beforeEnter: (to, from, next) => {
                if(!auth.can('Visualizar Clientes')) {
                    next({
                        path: '/403',
                    })
                } else {
                    next()
                }
            },
        },
        // Endereço
        {
            path: '/enderecos',
            name: 'enderecos',
            component: Endereco,
            linkExactActiveClass: 'active',
            beforeEnter: (to, from, next) => {
                if(!auth.can('Visualizar Endereços')) {
                    next({
                        path: '/403',
                    })
                } else {
                    next()
                }
            },
        },
    ];

    return routes;
}