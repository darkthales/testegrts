import Vue from 'vue';
const bus = new Vue();
import Toasted from 'vue-toasted';

export default {
	show(message, type, data={}){

		var config = {
			position: 'bottom-right',
			duration: '5000',
			closeOnSwipe: true,
			iconAuto: true
		};

		if(typeof data.position !== 'undefined'){
			config.position = data.position;
		}
		if(typeof data.duration !== 'undefined'){
			config.duration = data.duration;
		}
		if(typeof data.closeOnSwipe !== 'undefined'){
			config.closeOnSwipe = data.closeOnSwipe;
		}
		if(typeof data.iconAuto !== 'undefined'){
			config.iconAuto = data.iconAuto;
		}

		if(config.iconAuto){
			config['icon'] = 'check-circle';
			if(type == 'error'){
				config['icon'] = 'exclamation-triangle'; 
			}
		}

		bus.$toasted.show(message, {
		    type : type,
		    position : config.position,
		    duration : config.duration,
		    closeOnSwipe: config.closeOnSwipe,
		    icon: config.icon,
		    action : {
		        text : 'X',
		        onClick : (e, toastObject) => {
		            toastObject.goAway(0);
		        }
		    },
		});
	}
}



