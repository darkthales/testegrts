import GrtsMessage from './components/messages/message.js';
import GrtsValidation from './components/validation/validation.js';
import Datatable from './components/datatable/datatable.js';
// tosted
import Toasted from 'vue-toasted';
export default function GrtsComponent() {

	// Dependencias
	Vue.use(Toasted, {
	    iconPack : 'fontawesome'
	});;
	
	// Components
	Vue.component('GrtsModal', require('./components/modal/Modal.vue').default);
	Vue.component('GrtsLoadCircle', require('./components/load/LoadCircle.vue').default);
	Vue.component('GrtsPaginate', require('./components/pagination/Paginate.vue').default);
	Vue.component('showMessage', require('./components/messages/message.js').default);

	// Methods
	Vue.prototype.$grtsMessage = GrtsMessage;
	Vue.prototype.$grtsValidation = GrtsValidation;
	Vue.prototype.$datatable = Datatable;
}